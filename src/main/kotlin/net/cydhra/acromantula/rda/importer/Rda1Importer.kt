package net.cydhra.acromantula.rda.importer

import net.cydhra.acromantula.controller.model.WorkspaceEntry
import net.cydhra.acromantula.controller.model.database.META_DATA_KEY_ENCODING
import net.cydhra.acromantula.controller.model.database.META_DATA_KEY_TIME
import net.cydhra.acromantula.workspace.importer.ImporterStrategy
import net.cydhra.acromantula.workspace.importer.tools.ArchiveTreeBuilder
import org.apache.logging.log4j.LogManager
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.charset.Charset
import java.util.zip.Inflater
import kotlin.experimental.xor

object Rda1Importer : ImporterStrategy {
    private val logger = LogManager.getLogger()

    private const val RDA_HEADER_SIZE = 260
    private const val RDA_MAGIC_SIZE = 26
    private const val FILE_HEADER_SIZE = 276

    override fun canImport(file: File, fileContent: ByteArray): Boolean {
        if (fileContent.size < RDA_HEADER_SIZE)
            return false

        val header = fileContent.copyOfRange(0, RDA_HEADER_SIZE)
        deobfuscateRdaBlock(header)

        if (String(header.sliceArray(0 until RDA_MAGIC_SIZE), Charset.forName("UTF-8")) == "Crypted Resource File V1.1")
            return true

        return false
    }

    override fun import(fileName: String, fileContent: ByteArray): WorkspaceEntry {
        val header = fileContent.copyOfRange(0, RDA_HEADER_SIZE)
        deobfuscateRdaBlock(header)

        // extract file count
        val fileCount = ByteBuffer.wrap(header.sliceArray((RDA_HEADER_SIZE - 4) until RDA_HEADER_SIZE))
            .order(ByteOrder.LITTLE_ENDIAN)
            .getInt(0)
        logger.trace("File Count: $fileCount")

        // extract file headers
        val dictionary = fileContent.copyOfRange(RDA_HEADER_SIZE, RDA_HEADER_SIZE + fileCount * FILE_HEADER_SIZE)
        deobfuscateRdaBlock(dictionary)

        // construct archive tree
        val treeBuilder = ArchiveTreeBuilder(fileName)

        return transaction {
            for (i in (0 until fileCount)) {
                val dictionaryEntryContent =
                    dictionary.sliceArray((i * FILE_HEADER_SIZE) until ((i + 1) * FILE_HEADER_SIZE))
                val dictionaryEntry = RdaDictionaryEntry(dictionaryEntryContent)
                val inflater = Inflater()
                val archiveFileContent: ByteArray = if (dictionaryEntry.compressionFlag == 7) {
                    val compressedFileContent = fileContent.sliceArray(
                        dictionaryEntry.offset until (dictionaryEntry.offset +
                                dictionaryEntry.compressedFileSize)
                    )
                    inflater.setInput(compressedFileContent)
                    ByteArray(dictionaryEntry.decompressedFileSize).also { inflater.inflate(it) }
                } else {
                    fileContent.sliceArray(
                        dictionaryEntry.offset until (dictionaryEntry.offset +
                                dictionaryEntry.decompressedFileSize)
                    )
                }

                treeBuilder.addFileEntry(
                    dictionaryEntry.filename, archiveFileContent, META_DATA_KEY_TIME to
                            dictionaryEntry.timestamp.toString(), META_DATA_KEY_ENCODING to "UTF-8"
                )
            }

            return@transaction treeBuilder.create()
        }
    }

    /**
     * Decrypt an RDA archive block. The header and the dictionary block are obfuscated using a key-less stream
     * scrambler algorithm. This method can deobfuscate the headers by reverting the algorithm. Since the algorithm has
     * a state during execution, the full block without any additional padding must be the argument. The buffer is
     * descrambled in-place
     *
     * @param buffer the whole block that shall be deobfuscated.
     */
    private fun deobfuscateRdaBlock(buffer: ByteArray) {
        var keyState = 666666

        for (i in (0 until (buffer.size / 2))) {
            var key: Int = keyState * 0x343FD + 0x269EC3
            keyState = key
            key = (key shr 16) and 0x7FFF

            // the key is interpreted as a 16-bit-word, therefore two bytes are decrypted at once
            buffer[i * 2] = buffer[i * 2] xor (key and 0xFF).toByte()
            buffer[i * 2 + 1] = buffer[i * 2 + 1] xor ((key ushr 8) and 0xFF).toByte()
        }
    }

    /**
     * A parsed structure of a single dictionary entry of an RDA archive. Takes the byte array of the entry and
     * parses it into the structure
     *
     * @param block the entry content
     */
    private class RdaDictionaryEntry(block: ByteArray) {
        val filename: String
        val offset: Int
        val compressedFileSize: Int
        val decompressedFileSize: Int
        val compressionFlag: Int
        val timestamp: Int

        init {
            filename = String(block.sliceArray(0..255), Charset.forName("UTF-8"))
            with(ByteBuffer.wrap(block.sliceArray(256 until block.size)).order(ByteOrder.LITTLE_ENDIAN)) {
                offset = getInt(0)
                compressedFileSize = getInt(4)
                decompressedFileSize = getInt(8)
                compressionFlag = getInt(12)
                timestamp = getInt(16)
            }
        }
    }
}