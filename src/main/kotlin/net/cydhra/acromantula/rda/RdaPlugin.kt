package net.cydhra.acromantula.rda

import net.cydhra.acromantula.plugins.AcromantulaPlugin
import net.cydhra.acromantula.rda.importer.Rda1Importer
import net.cydhra.acromantula.workspace.importer.FileImporter
import org.apache.logging.log4j.LogManager

/**
 * A small plugin that adds an import strategy for RDA files. RDA is an obfuscated proprietary archive format used by
 * Sunflower Interactive for the game Anno 1701
 */
class RdaPlugin : AcromantulaPlugin {

    private val logger = LogManager.getLogger()

    override val name: String = "RDA Plugin"
    override val author: String = "Cydhra"

    override fun initialize() {
        FileImporter.registerImportStrategy(Rda1Importer)

        logger.info("loaded rda plugin")
    }

}